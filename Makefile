.PHONY: install
install:
	install -d $(DESTDIR)/etc/default
	install -m 644 default/hkiosk-run-video $(DESTDIR)/etc/default/
	install -d $(DESTDIR)/usr/lib/systemd/system
	install -m 644 systemd/hkiosk-run-video.service $(DESTDIR)/usr/lib/systemd/system/
	install -d $(DESTDIR)/usr/libexec
	install -m 755 libexec/hkiosk-run-video $(DESTDIR)/usr/libexec/
	install -d $(DESTDIR)/usr/share/hkiosk
	install -m 644 resources/hkiosk.png $(DESTDIR)/usr/share/hkiosk/
	-useradd --system --no-create-home --home /var/lib/hkiosk/.localstate --shell /sbin/nologin hkiosk
	-usermod --append --groups video hkiosk
	install -d $(DESTDIR)/var/lib/hkiosk
	install -d -o hkiosk -g hkiosk $(DESTDIR)/var/lib/hkiosk/.localstate

.PHONY: uninstall
uninstall:
	-rm $(DESTDIR)/etc/default/hkiosk-run-video
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-run-video.service
	-rm $(DESTDIR)/usr/libexec/hkiosk-run-video
	-rm $(DESTDIR)/usr/share/hkiosk/hkiosk.png
	-rmdir $(DESTDIR)/usr/share/hkiosk
	-rmdir $(DESTDIR)/var/lib/hkiosk/.localstate
	-rmdir $(DESTDIR)/var/lib/hkiosk
